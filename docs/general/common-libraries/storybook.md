---
sidebar_position: 3
---

# Coko Storybook

`@coko/storybook` gives you a zero-config storybook setup to work with. You can
check the code or open issue in [the gitlab repo](https://gitlab.coko.foundation/cokoapps/storybook).

## Get started

```bash
# Remove dependencies provided by this package
yarn remove \
  @storybook/react \
  @storybook/addon-docs

# Also remove any other storybook dependencies or addons you might have installed

# Install package and its peer dependencies
yarn add --dev @coko/storybook
```

To start, run the provided script:

```bash
yarn coko-storybook
```

Commonly, there is a `storybook` script defined in `package.json`, so that you
can simply run it with `yarn storybook`:

```json
{
  "scripts": {
    "storybook": "coko-storybook"
  }
}
```

This script will run storybook's webpack dev server with hot module reloading enabled.

## Usage guide

This package should provide everything you need to develop your UI components.
This means that you don't need to provide wrappers in order for the theme to exist or
to make components like `Link` that use `react-router-dom` work. They are provided
for you.

The default location for stories will be files that are in a `stories` folder in the
root of the repo. There's also a convention to be followed in that story files
should be named `<filename>.stories.<extension>`. Valid extensions are `js`, `mdx`
and `md`. The locations of story files can be customized (see `main.js` section below).

```bash
# will be automatically picked up
stories/book.stories.js
stories/manuscript.stories.mdx
stories/random.stories.md

# will not be picked up
myFolder/book.stories.js
stories/manuscript.js
stories/random.sh
```

### Extending configuration

#### Environment variables

The following environment variables are provided:

```bash
# Control which port storybook will open on
# Defaults to 5000
STORYBOOK_PORT

# Provide (relative to the root folder) path to the theme file
# Defaults to 'app/theme.js'
STORYBOOK_THEME_PATH
```

#### main.js

You can extend the config by creating a `.storybook/main.js` file. Whatever you
export from this file will be **merged** with the [existing main.js](https://gitlab.coko.foundation/cokoapps/storybook/-/blob/master/src/config/main.js#L6-19).

```js
// .storybook/main.js

// eg. Change where storybook should look for story files
module.exports = {
  stories: [`myCustomFolder/stories/**/*.stories.@(js|md|mdx)`],
};
```

#### preview.js

You can add functionality to the preview in storybook (the iframe that loads
your components) by adding a `./storybook/preview.js` file. This can customize
the behaviour of your components and is the appropriate place to add decorators
(to insert eg. context providers).
Keep in mind that [the default preview file](https://gitlab.coko.foundation/cokoapps/storybook/-/blob/master/src/config/preview.js)
will run anyway, as well as the custom one your provide.

```js
// .storybook/preview.js
import { addDecorator } from "@coko/storybook";

// wrap all components with a certain div for some reason
addDecorator((Story) => (
  <div id="wrap-it-all">
    <Story />
  </div>
));
```
