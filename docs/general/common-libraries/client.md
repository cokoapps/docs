---
sidebar_position: 2
---

# Coko Client

`@coko/client` should provide all the basics of having a client side app running.
You can find the repo [here](https://gitlab.coko.foundation/cokoapps/client).

## Get started

```bash
# Remove dependencies provided by this package

# It is likely this list is incomplete. You should also remove any webpack or
# babel plugins, presets and loaders that are not in the list below.
yarn remove \
  @pubsweet/client \
  @pubsweet/coko-theme \
  history \
  react-dom \
  apollo-client \
  graphql-tag \
  subscriptions-transport-ws \
  react-apollo \
  @apollo/react-hooks \
  @apollo/react-components \
  @apollo/react-hoc \
  @babel/core \
  @babel/preset-env \
  @babel/preset-react \
  babel-loader \
  babel-plugin-styled-components \
  babel-preset-minify \
  react-refresh \
  @pmmmwh/react-refresh-webpack-plugin \
  compression-webpack-plugin \
  copy-webpack-plugin \
  html-webpack-plugin \
  mini-css-extract-plugin \
  css-loader \
  html-loader \
  less-loader \
  style-loader \
  sass-loader \
  ts-loader \
  url-loader \
  webpack \
  webpack-cli \
  webpack-dev-server \
  uuid

# Install package and its peer dependencies
yarn add \
  @coko/client \
  @apollo/client \
  react \
  react-router-dom \
  styled-components \
  prop-types

```

## Usage guide

### Webpack

This package takes care of webpack compilation for you. That means there is no
need for maintaining webpack configs in your app any more. You can go ahead and
remove those. There are two scripts that are provided for dealing with webpack.
Use them in your `Dockerfile` or `package.json` scripts.

```bash
# This will bring up a webpack dev server. Use for development.
coko-client-dev

# This will create a static bundle. Use for production
coko-client-build
```

### Environment variables & configuration

The client defines a few environment variables. Some of them will only be used
by webpack in its config, and others will be passed on to the build itself.

#### Webpack config environment variables

Keep in mind that these environment variables **will not be passed down to the
build** as they only exist to control the webpack config itself.

```bash
# The absolute path of the base folder that webpack will compile
# Defaults to <root folder>/app
CLIENT_APP_ROOT_PATH

# The absolute path of the folder that webpack will compile into
# Defaults to <root folder>/_build
CLIENT_BUILD_FOLDER_PATH

# The absolute path of the folder that contains static assets
# Defaults to <root folder>/static
CLIENT_STATIC_FOLDER_PATH

# The (relative to CLIENT_APP_ROOT_PATH) path of the entry file of the app
# Defaults to ./start.js
CLIENT_ENTRY_FILE

# The (relative to CLIENT_APP_ROOT_PATH) path of the favicon icon
CLIENT_FAVICON_PATH

# Text to be displayed as a title within the tab
CLIENT_PAGE_TITLE

# The port that webpack dev server will run on. Valid only in development.
CLIENT_PORT

# Whether react's fast refresh / hot module reloading is enabled.
# Note that the page will reload if it is turned off, but will update to changes without reloading if on.
# Defaults to false
CLIENT_FAST_REFRESH
```

#### Build environment variables

These variables will be passed on to the build. This means that they are
accessible from within your client-side code.

```bash
# Valid values are 'development' and 'production'
# Defaults to 'development'
NODE_ENV

# Variables that are used to connect to the server
# Note that port is optional.
SERVER_PROTOCOL
SERVER_HOST
SERVER_PORT
```

#### Custom environment variables

If you need to pass environment variables to the build that are not covered
by the above, you will need to namespace them with `CLIENT_`.

For example:

```bash
# This will be passed to the build
CLIENT_MY_CUSTOM_VALUE

# This will NOT be passed to the build
MY_CUSTOM_VALUE
```

#### Config

Legacy client side code used to read values from the config folder. We're
moving away from that for a couple of reasons:

- Simplicity: Keep everything client-side to environment variables
- To avoid confusion with server-side configuration. We're moving towards having
  client and server-side code separated and it would be confusing to have a config
  folder that is used by both or to have two config folders.

The config folder (that is read by `node-config` via `pubsweet-server`) should
only be used by the server from now on. Make sure your client-side code does not
include lines like `import config from 'config'`.

### Base client component

There is a little bit of boilerplate that comes with initializing `react`.  
This package provides a helper to make that boilerplate go away.

```js
import { startClient } from '@coko/client'

const theme = { /* ...theme */ }
const routes = ( /* ...routes */ )

startClient(routes, theme)
```

This will take care of mounting react on the root div, initializing the theme
provider, wiring in the `history` dependency etc., so that you can focus on the
app instead of the infrastruture.

### Other exports

#### PageLayout

Sets the layout with a navigation bar on top and the different pages underneath.
Wrap your routes with it like in the example below:

```js
const routes = (
  <PageLayout fadeInPages navComponent={NavigationBar} padPages>
    <Switch>
      <Route component={First} exact path="/" />
      <Route component={Second} exact path="/second" />
    </Switch>
  </PageLayout>
);
```

#### theme

Provides a base theme to build on, so that you don't have to do it all from scratch.

```js
import { theme } from "@coko/client";

const myTheme = theme;
myTheme.colorPrimary = "magenta";

module.exports = myTheme;
```

#### serverUrl

Returns the url of the server

```js
import { serverUrl } from "@coko/client";

// eg. http://localhost:3000
console.log(serverUrl);
```

#### uuid

Generates a v4 uuid for you. Useful for faking data, creating unique ids for
`key` values in react arrays and more.

```js
import { uuid } from "@coko/client";

// eg. bcad5689-97e7-4f59-9016-753b0489cd27
console.log(uuid);
```
