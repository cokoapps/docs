---
sidebar_position: 2
---

# Team tools used within Coko

We try to keep most communication within the following platforms:

- [GitLab](https://gitlab.coko.foundation) for bugs/issues/feature requests
- [Discourse](https://discourse.coko.foundation) for technical Q&A
- [Mattermost](https://mattermost.coko.foundation) for ephemeral team discussions (you'll probably want to install the Mattermost desktop and/or mobile app).
- **8x8** for video chat. You can create any chat room, e.g. `https://8x8.vc/your_room_name_here`, but as these are open to anybody we name our chats `https://8x8.vc/coko/your_name_here` to help avoid collisions.

Once you've registered with GitLab, you can use your GitLab credentials for Discourse and Mattermost.
