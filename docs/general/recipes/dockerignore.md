---
sidebar_position: 3
---

# Sample `.dockerignore` file

```bash
# docker
Dockerfile*
docker-compose*

# git
.git
.gitignore

# npm & yarn
node_modules
npm-debug.log
package-lock.json
yarn-error.log

# misc
.devcontainer
.DS_Store
.vscode
app.pid

# environment files
.env*
*.env

# app
_build/
config/local*
data/
coverage/
cypress/
logs/
stories/
tmp/
uploads/

# Include README.md and LICENSE.md, exlcude the rest
*.md
!README.md
!LICENSE.md

# development config files
.eslintrc.js
.commitlintrc.js
.cz-config.js
.gitlab-ci.yml
.linstagedrc.js
.prettierrc.js
.stylelintrc.js
```

## Environment-specific `.dockerignore` file

You might run into situations where you want all this to be ignored in one
environment, but some of it to not be ignored in another. Common scenarios would
be not ignoring cypress tests in a CI environment, or wanting to keep the stories
folder in order to build a storybook container.

You can achieve this by adding a file named `<your-dockerfile-name>.dockerignore`.  
eg. If you have a `Dockerfile-ci` that is used in CI environments, add a file
named `Dockerfile-ci.dockerignore` and it will use that instead of the default
`.dockerignore`.
