# nginx

## Config file for nginx images

Use this config if you need to copy it into an `nginx` image that you've built
for production.

```conf
# SAME AS THE NGINX IMAGE'S DEFAULT CONF
# EXCEPT FOR THE LINE THAT IS NEEDED FOR REACT ROUTER

server {
    listen       80;
    server_name  localhost;

    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;

        #  => THIS LINE ENABLES SINGLE PAGE ROUTING WITH REACT ROUTER
        try_files $uri /index.html;
    }

    # redirect server error pages to the static page /50x.html
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
```
