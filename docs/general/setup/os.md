# Setting up your computer for development

If you're planning to use Windows make sure to read the [Windows section](/docs/general/setup/windows) before you continue.

## Dependencies

There's a few things you'll need installed on your OS before you start working on anything.

- git
- node & yarn
- docker & docker-compose
- cypress dependencies

If you're on a linux distribution and you want to run the apps locally (ie. without docker),
you might also need to install `build-essentials` or an equivalent package, in order to be able to compile
some of the dependencies. Some distributions come with these packages pre-installed, but others don't.

## git

Refer to the <a href="https://git-scm.com/book/en/v2/Getting-Started-Installing-Git" target="_blank">docs</a> to see how to install `git` on your specific OS.

## node & yarn

It's probably best to avoid installing `node` through your distribution's
package manager, as they tend to deliver versions that are outdated.

### nvm

It is recommended that you use `nvm` (node version manager) to be able to easily switch between `node` versions.  
Installation instructions can be found [here](https://github.com/nvm-sh/nvm#installing-and-updating).

After you've installed `nvm`:

Install `node 12` (the version our apps currently run on)

```
nvm install 12
```

Switch to `node 12` right now

```
nvm use 12
```

Set `node 12` to be the default

```
nvm alias default 12
```

### yarn

Now that you have `node` installed, you can simply install `yarn` through `npm`.

```
npm install --global yarn
```

Make sure you have version 1 installed. Version 2 is incompatible with version 1 and our apps.

```
yarn --version
```

If you used `nvm` to install node, keep in mind that this will install `yarn`
for that specific version of `node`. If you switch `node` versions you'll have
to install `yarn` for those versions as well (with the same command as above).

## docker

As with node, avoid using the linux distribution provided packages for `docker` and `docker-compose`.  
You can find the correct installation instructions for `docker` <a href="https://docs.docker.com/engine/install/" target="_blank">here</a> and for `docker-compose` <a href="https://docs.docker.com/compose/install/" target="_blank">here</a>.

If you're on Windows, make sure you install docker inside WSL (following the linux instructions from the link above), and _not the Windows desktop app_.

### Check that docker is working as expected

Make sure the docker daemon is running. On macOS this would simply mean that the app is running in the background.

On linux:

```
sudo service docker start
```

If you're running on a native linux distribution you can also enable the service to make sure it starts automatically on every boot.

```
sudo service docker enable
```

If you're on Windows/WSL, read the docker section in the Windows part of the docs.

Running the `info` command, you should now be able to see a long output of stats.

```
docker info
```

To check that `docker-compose` works as expected, the following command should return the current version:

```
docker-compose --version
```

### Troubleshooting

If you find that the `info` command only works with `sudo`, make sure your user is in the docker group.

You can check if your username is included in the output of the following command.

```
cat /etc/group | grep docker
```

If not, add yourself to the group.

```
sudo usermod -aG docker $USER
```

It is possible that a reboot or a logout is needed for this change to take effect.

## cypress

`cypress` is the end-to-end testing library we're currently using.

If you're on Linux, you might need to install some additional dependencies in order for it to run correctly.  
See <a href="https://docs.cypress.io/guides/continuous-integration/introduction.html#Dependencies" target="_blank">the relevant part of the documentation</a> for details.

If you're on WSL, you'll need an X Server installation so that `cypress` running inside the Linux VM can open a GUI on Windows.  
Follow <a href="https://nickymeuleman.netlify.app/blog/gui-on-wsl2-cypress" target="_blank">this guide</a> for further details.

## ssh

This is a good opportunity to <a href="https://www.atlassian.com/git/tutorials/git-ssh" target="_blank">set up an ssh key</a>, if you haven't set one up already.
