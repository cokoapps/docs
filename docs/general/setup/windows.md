# Setting up a development environment on Windows

If you're not going to be developing on a Windows machine, just skip to the [OS setup](/docs/general/setup/os) of the docs.

## WSL

Development directly on Windows is not supported. If you want to use Windows,
you will need to install the Windows Subsystem for Linux (WSL). This will set
up an optimized Linux VM inside your Windows installation. You can find
installation instructions <a
href="https://docs.microsoft.com/en-us/windows/wsl/install-win10"
target="_blank">here</a>.

Make sure you are running **WSL 2** (which comes with a full Linux kernel), otherwise
our apps won't work.

The instructions will use Ubuntu as a linux distribution (including installation commands).
Feel free to adjust if you want to use something different.

### Setup

- Install **WSL2**.
- From the Microsoft Store, install **Ubuntu** (most recent LTS version) and **Windows Terminal**. Restart.
- Run Ubuntu, **create default user**, then `sudo apt update`. <br/> _(you can run Ubuntu either by opening the "Ubuntu" program that you now have installed, or by opening Windows Terminal and choosing Ubuntu in the dropdown next to the tabs)_
- Install **Visual Studio Code** on _windows_. You can read more about this <a href="https://code.visualstudio.com/docs/remote/wsl" target="_blank">here</a>. Also see our [editor setup docs](/docs/general/setup/vscode).

Everything that is running on `localhost` inside WSL should be available on windows as well.  
eg. If your app is running on `localhost:4000` in Ubuntu, going to `localhost:4000` in your _windows_ browser should simply open the app.

### Docker

Do not use the desktop app that docker provides for Windows. Rather, install docker in the linux VM, as described in the [OS dependencies page](/docs/general/setup/os#docker).

You can start the **docker daemon** with `sudo service docker start`.  
This will need to be done every time you start a new VM (eg. if you reboot your system, or if you forcibly shutdown WSL).  
You can check if the docker daemon is running with the `docker info` command.

### Tips

- Ubuntu is not your only option for a linux distribution, but it is the one we've tried. Search for "linux" in the microsoft store if you want to see what else is available.
- Ubuntu will install its own terminal that you can open. It is highly recommended that you use Windows Terminal anyway.
- Windows Terminal can run more than Ubuntu (eg. a powershell environment, cmd).
  Use the dropdown next to the tabs to see your options. We recommend you set Ubuntu as the default environment in the settings, so that
  you don't have to choose it every time you open the terminal.
- Typing `code .` in the terminal will open Visual Studio Code for you in the current working directory.
- Typing `explorer.exe .` in the terminal will open windows' file explorer in the current working directory in the linux filesystem.
- If you use a database program that runs natively on Windows (eg. dbeaver) and it's failing to connect to your database via `localhost`, try using the IPV6 equivalent `[::1]`.

### Troubleshooting

#### VM memory usage

If the `vmmem` process (ie. the WSL the virtual machine) hogs too much memory (a [current known issue](https://github.com/microsoft/WSL/issues/4166)), you may wish to limit WSL memory usage.
In order to do this, create a `%UserProfile%\.wslconfig` file and adjust the maximum memory to whatever makes sense on your machine:

```
[wsl2]
memory=6GB
swap=0
localhostForwarding=true
```

#### Clock skew

WSL2 has a known issue (fixed in WSL2 kernel version 5.10.16.3) whereby its system clock pauses while your PC is hibernating. A large enough skew can lead to connectivity/credential and other errors. You can correct skew with `sudo hwclock -s`, or update your WSL2 kernel.

#### Releasing WSL storage

WSL2 accumulates but never releases storage on your hard drive (see [issue](https://github.com/microsoft/WSL/issues/4699)). Pruning your docker images, for instance, will not release any storage back to Windows. You can release storage manually following [these steps](https://superuser.com/a/1612289).
