import Image from "../../../src/custom/Image.js";

import EslintExampleImage from "../../../static/custom/eslint-example.png";
import StylelintExampleImage from "../../../static/custom/stylelint-example.png";
import PrettierExampleImage from "../../../static/custom/prettier-example.png";
import PrettierOnSave from "../../../static/custom/prettieronsave.gif";

# Editor Setup

This section will go through what you need to get up and running for development.

The following guide will assume you are using **Visual Studio Code**, as it is the
most common editor in the team.  
If you're using a different editor, you should find the equivalent packages/extensions
that will make this setup possible.

## Linter configuration

You shouldn't need to install linters (eg. `eslint`) globally, as they should
already be part of your project's `devDependencies` (via `@coko/lint`).  
It is however, necessary for you to have a correct linter configuration in your editor.  
You will not be able to commit code otherwise (because of our `lint-staged` setup).

Install the following extensions:

- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) _(for linting js code)_
- [stylelint](https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint) _(for linting CSS)_
- [Prettier - Code Formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) _(for formatting code)_

You might need to restart the editor for the extensions to take effect.  
You should now be able to see errors in your editor, like so:

Javascript (eg. cannot resolve package name, variable defined but not used)

<Image src={EslintExampleImage} maxWidth={550} />

CSS (eg. unknown property)

<Image src={StylelintExampleImage} maxWidth={400} />

Formatting (eg. multiple empty lines, parentheses around single argument)

<Image src={PrettierExampleImage} maxWidth={400} />

<p />

## Running prettier on save

You will also likely find it useful to run prettier on saving a file, so that
you don't need to manually correct formatting errors.  
To do so, type `CTRL+,` to
open your settings. Then search for `formatonsave` and check the
`Editor: Format On Save` checkbox.

It should work like this (here it's removing semicolons and adding a new line
at the end of the file when I type ctrl+s):

<Image src={PrettierOnSave} maxWidth={550} />

## Auto-fixing eslint errors on save

Not all errors are auto-fixable by `eslint`, but you can automate fixing the ones that
are (eg. empty line errors). Type `CTRL+,` to open your settings and search for
"code actions on save". This will present you with a link to edit this option
in your `settings.json` file. Click that and add the following:

```json
"editor.codeActionsOnSave": {
  "source.fixAll.eslint": true
}
```

## Syntax highlighting

Things like javascript or react are automatically highlighted by VSCode.  
But to add syntax highlighting to some less common formats, you might need one
of these plugins:

- [Graphql](https://marketplace.visualstudio.com/items?itemName=GraphQL.vscode-graphql)
- [vscode-styled-components](https://marketplace.visualstudio.com/items?itemName=jpoissonnier.vscode-styled-components)
- [YAML](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml)

## Working with markdown files

If you want to be able to preview your markdown files without pushing your code
or copy-pasting it to another tool, you will probably want to take a look at the
[Markdown Preview Enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced)
extension.

## Tips

- You can open a terminal within VSCode with the `` Ctrl-` `` shortcut - convenient during development.
