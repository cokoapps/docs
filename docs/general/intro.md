---
sidebar_position: 1
---

# Home

This website includes all needed **developer** documentation for working with
Coko apps and any affiliated technologies. This will include docs for our
shared libraries, guides on how to get set up for development, tips and tricks
that will make your life easier and more.

This website exists as a repo [here](https://gitlab.coko.foundation/cokoapps/docs).
Feel free to use the issues if you have corrections or suggestions.

**_Please note that this is currently being written, so there's more entries coming._**
