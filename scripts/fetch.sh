#!/bin/sh

set -e

TEMP_DIR=build_temp

mkdir -p ${TEMP_DIR}

rm -rf ${TEMP_DIR}/server

git clone \
  -b main \
  --single-branch \
  https://gitlab.coko.foundation/cokoapps/server.git \
  ${TEMP_DIR}/server

rm -rf docs/server
cp -r ${TEMP_DIR}/server/docs/docs/server docs/server

rm -rf build_temp
