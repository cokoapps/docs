const { eslint } = require('@coko/lint')

if (!eslint.settings) eslint.settings = {}

eslint.settings.jest = {
  version: 26,
}

eslint.rules['import/no-unresolved'] = [
  'error',
  { ignore: ['@docusaurus/*', '@theme/*'] },
]

module.exports = eslint
