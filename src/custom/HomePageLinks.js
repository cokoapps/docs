import React from 'react'
import clsx from 'clsx'

import styles from './homepageLinks.module.css'

const Links = () => {
  const data = [
    {
      label: '@coko/server',
    },
    {
      label: '@coko/client',
    },
    {
      label: '@coko/lint',
    },
    {
      label: '@coko/storybook',
    },
  ]

  return (
    <div className={clsx('root', styles.root)}>
      <div className={clsx('head', styles.head)}>Libraries</div>

      <div className={clsx('link-wrapper', styles.linkWrapper)}>
        {data.map(item => (
          <div className={clsx('link-item', styles.linkItem)}>{item.label}</div>
        ))}
      </div>
    </div>
  )
}

export default Links
