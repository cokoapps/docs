# BUILD
FROM node:20

RUN corepack enable

WORKDIR /home/node/app

COPY package.json .
COPY .yarnrc.yml .
COPY yarn.lock .

RUN yarn install

COPY . .

RUN yarn fetch
RUN yarn build

# SERVE
FROM nginx:alpine

COPY --from=0 /home/node/app/build /usr/share/nginx/html
